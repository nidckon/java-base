# Spis Lekcji
### Lekcja 1.1
0. Zaznajomienie się z potrzebnymi narzędziami (mvn, git, ide) oraz konfiguracja projektu.
0. Podstawowa struktura katalogów - dlaczego tak? czy to ma znaczenie?
0. PackageName - co to oznacza i z czym się to je?
   
### Lekcja 1.2
0. Podstawowa struktura "HelloWorld" - czyli witamy się na ekranie.
0. Pokazanie tekstu na różne sposoby.
   
### Lekcja 1 - Bonus
0. Budowanie i uruchamianie programu `standalone` (jar).

### Lekcja 2.1
0. Co to są zmienne?
0. Rodzaje zmiennych (prymitywne oraz nie) - tabelka dostępna [tutaj](./img/primitives_and_not.png).
   
### Lekcja 2.2
0. Zmienne podstawowe: boolean, char, String, int, double
0. Operacje na zmiennych.
   
### Lekcja 2.3
0. Operacje na zmiennych c.d.
0. Czytanie od użytkownika.

### Lekcja 2 - Bonus
0. Debug aplikacji - co można zyskać?

### Lekcja 3.1
0. Zmienna dodatkowa: float
0. Konwersja pomiędzy zmiennymi oraz boxing / unboxing prymitywów (obiekty).
   
### Lekcja 3.2
0. Stałe, statyczne (globalne) - kiedy należy, a kiedy nie powinno się z nich korzystać?
0. Biblioteka Math

### Lekcja 4.1
0. Tablice danych
   
### Lekcja 4.2
0. List oraz jego implementacje
   
### Lekcja 4.3
0. Set i Map oraz ich implementacje

### Lekcja 5.1
0. Instrukcja warunkowa IF - [if-else](./img/if_else.png)
0. **Zadanie** - Wczytać od użytkownika wartość i wyświetlić informacje (mniejsza, większa, równa) od liczby 10.
   
### Lekcja 5.2
0. Pętla FOR - [For](./img/loop.jpg)
0. **Zadanie** - Wyświetlić liczby w zakresie [1; 10]
0. **Zadanie** - Wyświetlić tylko liczby parzyste z zakresu podanego przez użytkownika

### Lekcja 5 - Bonus
0. Bytecode aplikacji - czyli plik java, a plikk class

### Lekcja 6.1
0. Blok warunkowy (wyboru) SWITCH - [switch](./img/switch.jpg)
0. **Zadanie** - Wczytać wartość od użytkownika i wyświetlić informacje, czy równe (3, 5, 7, 10) lub żadne z tych
   
### Lekcja 6.2
0. Pętla WHILE - [while](./img/while-loop.png)
0. **Zadanie** - Wyświetlić liczby w zakresie [1; 10] jak dla FOR.
0. **Zadanie** - sumować liczby wpisywane przez użytkownika, aż poda wartość 0.

### Lekcja 7.1
0. Krótko o wyjątkach.
0. [Rodzaje wyjątków](./img/exceptions.png)
   
### Lekcja 7.2
0. Czytanie informacji z pliku i zapisywanie do niego.
0. **Zadanie** - Użytkownik podaje ścieżkę do czytanego pliku i nazwę pliku wynikowego.
   
### Lekcja 7.3
0. Czytanie z zasobów aplikacji (głównych i dostępnych dla paczki).
0. **Zadanie** - Użytkownik podaje, który zasób ma zostać odczytany i pod jaką nazwą ma być zapisany
0. **BIBLIOTEKA** Czytanie z pliku przy pomocy Apache.

### Lekcja 8.1
0. Definicja klasy
0. Własna klasa (Adder)
0. **Zadanie** - Utworzyć inne klasy z podstawowych działań matematycznych (+-*/).

### Lekcja 8.2
0. Modyfikatory dostępu (hermetyzacja) - private, public
0. Settery i Gettery
   
### Lekcja 8.3
0. Rekurencja - odwoływanie się do samego siebie
0. **Zadanie** sumowanie liczb z listy
0. **Zadanie** ciąg fib
0. **Zadanie** lista plików w folderach zagnieżdżonych (np. szukanie po nazwach plików)

### Lekcja 9.1
0. Co to jest enum i do czego służy?
0. **Zadanie** - Utworzyć enum z listą kategorii filmowych

### Lekcja 9.2
0. Co to interfejs, jakie są rodzaje i co nam dają?
0. **Zadanie** - Zmienić klasy utworzone w poprzednich lekcjach, aby korzystały z interfejsu.

### Lekcja 10
0. Klasa abstrakcyjna
0. Pozostałe modyfikatory dostępu - protected, default
0. **Zadanie** Utwórz abstrakcyjną klasę Calculator
0. **Zadanie** Bazując na klasie abstrakcyjnej, utwórz 2 klasy 
    - HistoryCalculator - przechowuje również historię wyników / operacji 
    - OneFormCalculator - przyjmuje 1 klasę (np. Adder) i wykonuje obliczenia właśnie na niej

### Lekcja 11.1
0. Testujemy nasze twory - jUnit
0. Asercje - czyli co chcemy sprawdzać
   
### Lekcja 11.2
0. Logujemy dane - Slf4j, Logger
0. Uczymy się czytać dane z wyjątków i logów
   
### Lekcja 11.3
0. Własne wyjątki
0. Programowanie przez wyjątki - zalety, wady i jak to robić?

### Projekt - Lekcja 12
0. Wybranie własnego projektu (niezbyt trudnego), może być istniejący (np. kalkulator / notatnik / archiwizator)
0. Ustalenia, co będzie potrzebne (funkcjonalności)
0. Ustalenia, co można wykorzystać, aby wykonać (funkcjonalności)

### Projekt - Lekcja 13.1
0. Realizacja wydmuszki - menu / przemieszczania się
0. Realziacja co najmniej 1 funkcjonalności
0. Prezentacja projektu, rozmowa o rozwiązaniach

### Projekt - Lekcja 13.2
0. Prezentacja projektu - c.d.

### Lekcja 14
0. **Swing** - prompt, czyli wyskakujące okienka (komunikaty, odczyt i zapis plików)

### Lekcja 15
0. **Swing** - własne okno
0. **Swing** - szablon okna, w tym reagowanie na przyciski

### Lekcja 16
0. **Swing** - menu górne aplikacji i obsługa
0. **Swing** - menu kontekstowe i obsługa

### Lekcja 17
0. **Swing** - c.d.

# [Powrót](../README.md)
