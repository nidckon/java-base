### Lista komend GIT
1. Pobranie projektu:
    - `git clone [PROJECT_HOST]`
    - np. `git clone https://bitbucket.org/nidckon/java-base.git`
2. Aktualizacja wersji oraz aktualnej **gałęzi**:
    - `git fetch` - ściąga informacje o zmianach
    - `git pull` - ściąga informacje o zmianach i zmiany oraz aktualizuje aktualną **gałąź**

##### Pozostałe dane nie będą nam potrzebne na zajęciach.

3. Przełączanie się pomiędzy gałęziami:
    - `git checkout [BRANCH]` - przełączenie się na **gałąź** o nazwie BRANCH
    - np. `git checkout master` - przełączenie się na główną **gałąź**
    - np. `git checkout lekcja-1` - przełączenie się na **gałąź** o nazwie 'lekcja-1'
4. Zachowanie swoich zmian (utworzonych plików, zmodyfikowanych danych):
    - `git commit -a -m "moje pliki"` - zachowuje Twoje zmiany z aktualnej **gałęzi** właśnie na niej, po powrocie do tej lekcji dalej będziesz miał do nich dostęp
5. Wywalenie swoich zmian, które nie były zachowywane:
    - `git reset --hard` - usuwa wszystkie zmiany, które nie były zachowane
6. Przechowywanie wszystkich swoich zmian na głównej **gałęzi** (należy wcześniej zachować zrobione zmiany):
    - `git checkout master && git merge lekcje/lekcja-<NR>` - gdzie `<NR>` to numer Twojej aktualnej lekcji, np. `1`
7. Tymczasowe schowanie naszych zmian:
    - `git stash` - odłożenie naszych zmian na bok (na stos)
    - `git stash apply` - przywrócenie ostatnio odłożonych zmian

# [Powrót](../README.md)
