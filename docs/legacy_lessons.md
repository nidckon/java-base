# Spis Lekcji
### Lekcja 1  (lekcje/lekcja-1)
0. Zaznajomienie się z potrzebnymi narzędziami (mvn, git, ide) oraz konfiguracja projektu.
0. Podstawowa struktura katalogów - dlaczego tak? czy to ma znaczenie?
0. PackageName - co to oznacza i z czym się to je?
0. Podstawowa struktura "HelloWorld" - czyli witamy się na ekranie.

### Lekcja 2 (lekcje/lekcja-2)
0. Co to są zmienne?
0. Rodzaje zmiennych (prymitywne oraz nie) - tabelka dostępna [tutaj](./img/primitives_and_not.png).
0. Konwersja pomiędzy zmiennymi oraz boxing / unboxing prymitywów (obiekty).
0. Stałe, statyczne (globalne) - kiedy należy, a kiedy nie powinno się z nich korzystać?

### Lekcja 3 (lekcje/lekcja-3)
0. Operacje na zmiennych.
0. Pętle i bloki warunkowe:

     - [For](./img/loop.jpg), 
     - [while](./img/while-loop.png), 
     - [switch](./img/switch.jpg) oraz 
     - [if-else](./img/if_else.png)

### Lekcja 4 (lekcje/lekcja-4)
0. Komunikacja z człowiekiem - czytanie z konsoli.
0. Czytanie informacji z pliku i zapisywanie do niego.
0. Czytanie z zasobów aplikacji (głównych i dostępnych dla paczki).
0. Krótko o wyjątkach.

### Lekcja 5 (lekcje/lekcja-5)
0. Metody - czyli ułatwiamy sobie życie.
0. Klasy - dzielimy ze wzglęu na przeznaczenie oraz
     - konstruktory - czyli inicjalizacja obiektów oraz blokada ich tworzenia,
     - settery i gettery

### Lekcja 6 (lekcje/lekcja-6)
0. Klasyfikatory pól w klasach:
     - public
     - private
     - protected
0. Klasy abstrakcyjne i rozszerzanie
0. Interfejsy i implementacja
0. Testujemy nasze klasy.

### Lekcja 7 (lekcje/lekcja-7)
0. [Rodzaje wyjątków](./img/exceptions.png)
0. Własne wyjątki
0. Czytanie logów - co można zrozumieć z wyjątków i jak na nie patrzeć?
0. Programowanie przez wyjątki - zalety, wady i jak to robić?

### Lekcja 8 (lekcje/lekcja-8)
0. Projekt: `Wypasiony Kalkulator`
     - możliwość ciągłego wykonywania operacji, aż do wybrania `oblicz`
     - możliwość operacji na liczbach całych oraz zmiennoprzecinkowych
     - możliwość obliczania:
        - dodawania
        - odejmowania
        - mnożenia
        - dzielenia
        - potęgowania
     - możliwość cofnięcia operacji
        - należy przechowywać historię wartości oraz operacji
     - możliwość powtórzenia operacji (po cofnięciu)
     - możliwość duplikacji - kopia ostatniej operacji

### Lekcja 9 (lekcje/lekcja-9)
0. Projekt: `Baza użytkowników`
     - przechowujemy listę użytkowników w zewnętrznym pliku
     - przechowujemy dane użytkownika:
        - imię i nazwisko
        - login
     - zezwalamy na operacje:
        - dodanie nowego użytkownika
        - usunięcie istniejącego użytkownika
        - modyfikacje danych użytkownika
             - zatwierdzenie edycji
             - anulowanie edycji
     - podgląd listy użytkowników jest dostępny dla każdego
        - lista zawiera tylko `imię i nazwisko`
     - edycja listy (dodaj, usuń, edytuj) dostępna tylko dla zalogowanego
        - logujemy się podając `login`

### Lekcja 10 (lekcje/lekcja-10)
0. Projekt: `System kucharski`
     - lista możliwych składników w zasobach
     - dane składnika:
        - nazwa
        - `miarka`
     - lista możliwych przypraw w zasobach
     - dane przyprawy:
        - nazwa
        - typ (posiekane / sproszkowane / w kulkach)
        - `miarka`
     - dane `miarki`:
        - typ (kg / ml / szklanka / łyżeczka)
        - wartość (1.5, 2)
     - możliwość doczytania zewnętrznej listy składników i przypraw:
        - opcja `użyj innych przypraw` oraz `użyj innych składników`
     - lista `2 przepisów domyślnych`
        - korzystają tylko ze składników i przypraw z zasobów
     - możliwość utworzenia swojego przepisu

# [Powrót](../README.md)
