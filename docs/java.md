# Java
W zależności od wybranej opcji:

0. Postępujemy według wskazówek instalatora (możemy pominąć resztę informacji w tej sekcji).
0. Musimy postąpić adekwatnie jak przy instalacji `maven`. Instrukcja poniżej.

Należy skopiować zawartość archiwum JRE/JDK (nie zmieniając nazw!) do folderu `java` w `Program Files`.
Następnie należy dodać dostęp w zmiennych środowiskowych (aby `java` oraz `javac` były dostępnę z poziomu konsoli windows).

Wartości, które muszą być dodane to pełne ścieżki do folderu `bin` w naszym `jdk1.8.0_xxx` oraz `jre1.8.0_xxx`. 
Przykładowe wartości:

- `C:\Program Files\Java\jdk1.8.0_191\bin` - JDK
- `C:\Program Files\Java\jre1.8.0_191\bin` - JRE

Instrukcja dodawania wartości do zmiennych środowiskowych znajduje się [tutaj](./add-system-variables.md).

# [Powrót](../README.md)
