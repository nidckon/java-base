# Instrukcja dodawania zmiennych środowiskowych
## Instrukcja jest uniwersalna i należy postępować dla każdego oprogramowania tak samo
#### Można postępować według instrukcji opisanej dla javy, która znajduje się [tutaj](https://www.java.com/pl/download/help/path.xml).
##
#### Można również skorzystać z listy poniżej. Lista ta jest taka sama, jak podano pod powyższym linkiem.
---
![moj komputer - wlasciwosci](./windows/my-computer-properties.PNG)  

---
0. Należy wejść we **właściwości** `Mój Komputer`.  
0. Wybrać pole `Zaawansowane ustawienia systemu`.  
---
![wlasciwosci zaawansowane](./windows/advanced-properties.PNG)  

---
0. Przejść do zakładki `Zaawansowane`.  
0. Kliknąć przycisk `Zmienne środowiskowe`.  
---
![edycja zmiennej systemowej](./windows/path.PNG)  

---
0. Następnie w sekcji `zmienne systemowe` należy poszukać wpisu o nazwie `PATH`.  
0. Należy kliknąć wpis oraz wybrać `Edytuj`  
 W zależności od systemu operacyjnego możemy mieć inne okno:  
      - lista zmiennych w 1 obszarze tekstowym:
         0. Należy na początku i końcu naszej wartości dodać znak `"`.
         0. Na samym końcu obszaru tekstowego należy dodać `;` oraz wkleić naszą zmodyfikowaną wartość.
    
      - listę zmiennych znajdujących się 1 pod drugą:
         0. Kliknąć przycisk `nowa`.
         0. Wkleić naszą ścieżkę.  
    
7. Zatwierdzić wszystkie okna.  
8. Zamknąć wszystkie okna konsoli, jeśli były otwarte.

# [Powrót](../README.md)
