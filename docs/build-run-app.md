# Informacje o budowaniu aplikacji standalone znajdziesz

Struktura programu:
```
- src
-   - main
-   -   - java
-   -   -   - pl
-   -   -   -   - sample 
-   -   -   -   -   - project
-   -   -   -   -   -   - JakasNazwa.java
-   -   - resources
-   - test
-   -   - java
-   -   - resources
- pom.xml
```

## Budowanie aplikacji
1. Przejdź do głównego katalogu swojego programu (tam, gdzie jest plik `pom.xml` i katalog `src`).
2. Otwórz `terminal` w tym miejscu.

Teraz są 2 opcje, którymi można zbudować nasz program do 1 pliku JAR, który później możemy uruchomić.

### Za pomocą MAVENa
1. Komenda: `mvn clean package`
- `clean` - wyczyści folder z poprzedniego budowania aplikacji
- `package` - zbuduje aplikację oraz spakuje ją do pliku JAR.

### Za pomocą JAVAC
1. Utwórz folder o nazwie `target` lub dowolnej innej, w głównym folderze projektu.
2. Komenda: `javac -d ./target -s ./src/main/java/pl/sample/project/JakasNazwa.java`
    - `-d ./target` - wskazuje miejsce, gdzie mają znaleźć się skompilowane pliki
        - `kompilacja` - przetwarzanie kodu zrozumiałego przez programistę na kod zrozumiały przez maszynę
    - `-s ./src/main/java/pl/sample/project/JakasNazwa.java` - jakie pliki `JAVA` mają zostać skompilowane
3. Dalej przejść jeśli nie było błędów.
4. Przejdź do folderu ze skompilowanymi klasami - `cd target`
5. Komenda: `jar cf SampleProject.jar ./pl/sample/project/JakasNazwa.class`
    - `cf SampleProject.jar` - wskazuje nazwę i miejsce pliku `JAR`
    - `./pl/sample/project/JakasNazwa.class` - wskazuje skompilowane pliki `JAVA`


## Uruchamianie aplikacji

Bez względu na wybraną ścieżkę przy kompilacji, uruchamianie będzie takie samo (dla powyższego przykładu).

### Uruchamianie za pomocą JAVA
1. Przejdź do folderu, w którym znajduje się plik `JAR`.
2. Uruchom terminal w tym folderze.
3. Komenda: `java -cp SampleProject.jar pl.sample.project.JakasNazwa`
    - `-cp SampleProject.jar` - wskazuje, że chcesz uruchomić jakiś plik `CLASS` z archiwum `JAR`
    - `pl.sample.project.JakasNazwa` - wskazuje `package` dla Twojej klasy oraz nazwę pliku `JakasNazwa` bez rozszerzenia

## Bonus
Aby nie wskazywać wprost klasy, którą chcemy uruchomić można to zrobić na 2 sposoby:
1. Zmodyfikować plik `MANIFEST`, który znajduje się w archiwum `JAR`.
    1. Dodaj w dowolnym miejscu jako nowa linia: `Main-Class: pl.sample.project.JakasNazwa`
2. Dobrze skonstruować plik `pom.xml` przy budowaniu poprzez `mvn`:
```
<project>

    // ...

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>pl.sample.project.JakasNazwa</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

Po takiej zmianie, uruchomić nasz program możemy za pomocą tej komendy:
`java -jar SampleProject.jar`

# [Powrót](../README.md)
