var qTypyZmiennych = [
    {
        question: 'Które z poniższych to prymitywy?',
        type: 'multi',
        answers: [
            {answer: 'int', good: true},
            {answer: 'boolean', good: true},
            {answer: 'Integer', good: false},
            {answer: 'Boolean', good: false}
        ]
    },
    {
        question: 'Czy String jest prymitywem?',
        type: 'single',
        answers: [
            {answer: 'Tak', good: false},
            {answer: 'Nie', good: true}
        ]
    },
    {
        question: 'Czy double i float jest tym samym?',
        type: 'single',
        answers: [
            {answer: 'Tak', good: false},
            {answer: 'Nie', good: true}
        ]
    },
    {
        question: 'Czy double i Double jest tym samym?',
        type: 'single',
        answers: [
            {answer: 'Tak', good: false},
            {answer: 'Nie', good: true}
        ]
    },
    {
        question: 'W jaki sposób tworzymy zmienną z domyślną wartością?',
        type: 'single',
        answers: [
            {answer: 'zmienna int = 10;', good: false},
            {answer: 'zmienna int;', good: false},
            {answer: 'int zmienna = new int;', good: false},
            {answer: 'int zmienna = 0;', good: false},
            {answer: 'int zmienna;', good: true}
        ]
    },
    {
        question: 'Czy istnieje typ Number, albo number?',
        type: 'single',
        answers: [
            {answer: 'Tak', good: false},
            {answer: 'Nie', good: true}
        ]
    },
    {
        question: 'Czy każdy prymityw ma swój nieprymitywny (obiektowy) odpowiednik?',
        type: 'single',
        answers: [
            {answer: 'Tak', good: true},
            {answer: 'Nie', good: false}
        ]
    },
    {
        question: 'Które z poniższych przechowują wartość logiczną?',
        type: 'multi',
        answers: [
            {answer: 'int', good: false},
            {answer: 'boolean', good: true},
            {answer: 'String', good: false},
            {answer: 'Boolean', good: true}
        ]
    },
    {
        question: 'Które z poniższych przechowują jakiekolwiek wartości liczbowe?',
        type: 'multi',
        answers: [
            {answer: 'int', good: true},
            {answer: 'Integer', good: true},
            {answer: 'double', good: true},
            {answer: 'float', good: true}
        ]
    },
    {
        question: 'Wartość logiczna "prawda" to:',
        type: 'multi',
        answers: [
            {answer: 'true', good: true},
            {answer: 'false', good: false},
            {answer: '1', good: false},
            {answer: '0', good: false}
        ]
    },
    {
        question: 'Która z wartości poniższych to poprawna wartość dla typu "float"?',
        type: 'multi',
        answers: [
            {answer: '3.f', good: true},
            {answer: '3.14f', good: true},
            {answer: '.14f', good: true},
            {answer: '3.14', good: false}
        ]
    },
    {
        question: 'Która z wartości poniższych to poprawna wartość dla typu "char"?',
        type: 'multi',
        answers: [
            {answer: '\'\\t\'', good: true},
            {answer: '\'\\n\'', good: true},
            {answer: '\'n\'', good: true},
            {answer: '\'char\'', good: false},
            {answer: '96', good: true},
        ]
    }
];
