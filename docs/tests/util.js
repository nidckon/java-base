Array.prototype.shuffle = function() {
    for (let i = this.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
}

Array.prototype.each = function(operation) {
    for (let i = 0; i < this.length; ++i) {
        operation(this[i], i);
    }
}

function createInput(type, optionName, id, answer) {
    var input = document.createElement('input');
    input.type = type;
    input.name = optionName;
    input.id = optionName + '-' + id;
    input.value = answer.answer;
    input.answer = answer;
    return input;
}

function createLabel(id, answer, option) {
    var label = document.createElement('LABEL')
    label.for = id;
    label.innerText = answer;
    label.appendChild(option);
    return label;
}
