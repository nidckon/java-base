var app = {
    tests: [
        {name: 'Typy zmiennych', model: 'qTypyZmiennych'},
        {name: 'Instrukcje warunkowe', model: 'qInstrukcjeWarunkowe'},
    ],
    containers: {
        links: '#menu-links',
        questions: '#content-questions',
        check: '#check',
        reload: '#reload'
    },
    questionNameLimiter: 1,
    lastQuestionList: []
};

window.addEventListener('load', () => {
    var container = document.querySelector(app.containers.links);
    app.tests.each(test => {
        var option = document.createElement('LI');
        option.innerText = test.name;
        option.onclick = () => chooseQuestionList(test.model);
        container.appendChild(option);
    })
});

function reset() {
    document.querySelector(app.containers.questions)
        .classList.add('hide');
    document.querySelector(app.containers.check)
        .classList.add('hide');
    document.querySelector(app.containers.reload)
        .classList.add('hide');
    document.querySelector(app.containers.questions)
        .innerHTML = '';

    document.querySelector(app.containers.links)
        .classList.remove('hide');
}

function showQuestionList() {
    document.querySelector(app.containers.links)
        .classList.add('hide');
    document.querySelector(app.containers.questions)
        .classList.remove('hide');
    document.querySelector(app.containers.reload)
        .classList.remove('hide');
    document.querySelector(app.containers.check)
        .classList.remove('hide');
}

function createQuestionContainer(question) {
    var li = document.createElement('LI');
    li.question = question;
    var title = document.createElement('P');
    title.innerText = question.question;
    li.appendChild(title);
    return li;
}

function prepareMulti(li, optionName, a, id) {
    var checkbox = createInput('checkbox', optionName, id, a);
    var label = createLabel(checkbox.id, a.answer, checkbox);
    li.appendChild(label);
}

function prepareSingle(li, optionName, a, id) {
    var radio = createInput('radio', optionName, id, a);
    var label = createLabel(radio.id, a.answer, radio);
    li.appendChild(label);
}

function prepareQuestionContainer(questions) {
    var container = document.querySelector(app.containers.questions);
    container.innerHTML = '';
    questions.shuffle()
        .slice(0, 4)
        .each(q => {
            var li = createQuestionContainer(q);
            var optionName = 'question-' + (app.questionNameLimiter++);
            var shuffled = q.answers.shuffle();
            if (q.type === 'single') {
                shuffled.each((a, id) => prepareSingle(li, optionName, a, id));
            } else {
                shuffled.each((a, id) => prepareMulti(li, optionName, a, id));
            }
            container.appendChild(li);
        });
}

function chooseQuestionList(name) {
    var questions = [...eval(name)];
    app.lastQuestionList = [...questions];
    prepareQuestionContainer(questions);
    showQuestionList();
}

function checkSingle(li) {
    li.className = '';
    var checked = Array.from(li.querySelectorAll('input:checked'));
    if (checked.length === 0) {
        li.classList.add('wrong');
    } else {
        checked.each(a => {
            if (a.answer.good) {
                li.classList.add('good');
            } else {
                li.classList.add('wrong');
            }
        });
    }
}

function checkMulti(li) {
    Array.from(li.querySelectorAll('input'))
        .each(a => {
            a.parentElement.className = '';
            if (a.answer.good && !a.checked || !a.answer.good && a.checked) {
                a.parentElement.classList.add('wrong');
            } else if (a.answer.good && a.checked) {
                a.parentElement.classList.add('good');
            }
        });
}

function checkAnswers() {
    Array.from(document.querySelectorAll(app.containers.questions + ' li'))
        .each(li => {
            var q = li.question;
            if (q.type === 'single') {
                checkSingle(li);
            } else {
                checkMulti(li);
            }
        });
}

function reload() {
    prepareQuestionContainer(app.lastQuestionList);
    showQuestionList();
}