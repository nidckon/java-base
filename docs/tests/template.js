var qTemplate = [
    {
        question: 'Question for many answers?',
        type: 'multi',
        answers: [
            {answer: 'a1', good: true},
            {answer: 'a2', good: true},
            {answer: 'a3', good: false},
            {answer: 'a4', good: false}
        ]
    },
    {
        question: 'Question for conrecte answer?',
        type: 'single',
        answers: [
            {answer: 'a1', good: false},
            {answer: 'a2', good: true}
        ]
    }
];
