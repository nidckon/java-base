var qInstrukcjeWarunkowe = [
    {
        question: 'Która z poniższych to poprawna składnia instrukcji warunkowej',
        type: 'single',
        answers: [
            {answer: 'if (warunek) { instrukcje_do_wykonania }', good: true},
            {answer: 'if warunek { instrukcje_do_wykonania }', good: false},
            {answer: 'if warunek instrukcje_do_wykonania', good: false},
            {answer: 'if (warunek, instrukcje_do_wykonania)', good: false},
            {answer: 'if warunek ( instrukcje_do_wykonania )', good: false}
        ]
    },
    {
        question: 'Jaki będzie efekt wykonania poniższego kodu: if (true) { System.out.print("Pokaz tekst); }',
        type: 'single',
        answers: [
            {answer: 'Pokaże się tekst "Pokaz tekst"', good: true},
            {answer: 'Nic się nie wyświetli', good: false},
            {answer: 'Będzie błąd wykonania', good: false}
        ]
    },
    {
        question: 'Które z poniższych kodów wykona instrukcje, gdy "warunek" nie został spełniony?',
        type: 'multi',
        answers: [
            {answer: 'if (!warunek) { instrukcje }', good: true},
            {answer: 'if (warunek) { } else { isntrukcje }', good: true},
            {answer: 'if (warunek) { instrukcje }', good: false},
            {answer: 'if (not warunek) { instrukcje }', good: false},
            {answer: 'ifnot (warunek) { instrukcje }', good: false},
            {answer: 'if (warunek) { } else if (!warunek) { instrukcje }', good: true}
        ]
    },
    {
        question: 'Czy instrukcje warunkowe mogą być łączone w bloki instrukcji warunkowych?',
        type: 'single',
        answers: [
            {answer: 'tak', good: true},
            {answer: 'nie', good: false}
        ]
    },
    {
        question: 'Które słowo kluczowe zezwala na wykonanie instrukcji, gdy warunek nie jest spełniony?',
        type: 'single',
        answers: [
            {answer: 'else', good: true},
            {answer: 'alternative', good: false},
            {answer: 'opposite', good: false}
        ]
    },
];
