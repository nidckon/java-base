# maven
Należy przekopiować całą zawartość archiwum (folder `apache-maven-3.x.x`) do `Program Files`.
Następnie należy dodać dostęp w zmiennych środowiskowych (aby `maven` był dostępny z poziomu konsoli windows).

Wartość, która musi być dodana to pełna ścieżka do folderu `bin` w naszym `apache-maven-3.x.x`. 
Przykładowa wartość: `C:\Program Files\apache-maven-3.6.3\bin`

Instrukcja dodawania wartości do zmiennych środowiskowych znajduje się [tutaj](./add-system-variables.md).

# [Powrót](../README.md)
