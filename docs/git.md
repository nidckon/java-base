# GIT
Należy postępować według instrukcji instalatora.
Uwagi:
- integracja z eksploratorem windows - **nie jest wymagana**,
- skróty na pulpicie i w menu start - **nie są wymagane**,
- edytor plików GIT - najlepiej skorzystać z `notepad++` lub 
`zwykły notatnik` (powinien znajdować się on pod ścieżką: `"C:\Windows\System32\notepad.exe"`)

Po instalacji należy uruchomić terminal a następnie wpisać poniższe komendy:
- `git config --global user.email "EMAIL"` - w miejsce `EMAIL` należy podać `dowolny email` (jeśli posiadasz konto na platformie github / bitbucket zalecam podanie tamtego maila)
- `git config --global user.name "NAME"` - w miejsce `NAME` należy podać `nazwę` pod którą Twoje zmiany mają być widoczne w systemie wersji

Powyższe ustawienia są niezbędne tylko w momencie, kiedy chcesz zachowywać swoje zmiany w projekcie.
Jeśli nie chcesz korzystać z tego mechanizmu - nie jest to potrzebne, ale w momencie pierwszego użycia wyskoczy prośba o uzupełnienie tych informacji.

Wprowadzone w ten sposób dane nie są stałe i będzie można je zmienić. Zarówno ustawienia globalne (dla nowych projektów z kontrolą wersji GIT) jak i dla konkretnych projektów (wyjątki).


# [Powrót](../README.md)
