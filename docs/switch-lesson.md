### Przełączanie pomiędzy lekcjami
**Nasze zajęcia będziemy prowadzić zawsze na głównej gałęzi.**

Wcześniej jednak były utworzone zajęcia na zasadzie kolejnych, gotowych lekcji. Aby przejść do konkretnej lekcji, należy wpisać komendę:

`git checkout legacy/lekcje/lekcja-<NR>`, gdzie `<NR>` to numer lekcji, np. `1`.

Lekcje były robione przyrostowo. Oznacza to, że wszystkie pliki z poprzednich lekcji znajdują się też w każdych następnych.

**Nie należy!** modyfikować plików w części `informacje`. Aby utworzyć swoje pliki związane z lekcją, należy skorzystać z miejsca specjalnie do tego przeznaczonego, czyli folderu `lekcje`.

# [Powrót](../README.md)
