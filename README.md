# Projekt - podstawy programowania - Java

## Przygotowania
### Potrzebne oprogramowanie:
1. Środowisko programistyczne (IDE):
    - [IntelliJ Community Edition](https://www.jetbrains.com/idea/download/)
    - [netBeans](https://netbeans.apache.org/download/index.html)
2. Automatyzacja budowania, zależności oraz organizacji projektu:
    - [maven](http://ftp.ps.pl/pub/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip)
3. Java v8 - Java Development Kit (JDK) + Java Runtime Environment (JRE):
    - [Oracle JDK Unregistered](https://download.java.net/openjdk/jdk8u41/ri/openjdk-8u41-b04-windows-i586-14_jan_2020.zip) + [JRE Unregistered](https://www.java.com/pl/download/windows-64bit.jsp)
    - [AdoptOpen](https://adoptopenjdk.net/releases.html) - JDK + JRE
    - [Oracle JDK](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html) + [Oracle JRE](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)
4. Kontrola wersji projektów:
    - [git](https://git-scm.com/downloads)
    
### Uwagi do instalacji powyższego oprogramowania

#### IDE - [click](./docs/ide.md)
- polecam Intellij, wersja community jest w pełni wystarczalna
- na intellij będą przedstawiane dalsze elementy (skróty klawiszowe mogą być różne dla innych IDE)

#### maven - [click](./docs/maven.md)
- jest potrzebny do konfiguracji naszego projektu
- będzie on wykorzystywany w większym stopniu od połowy zajęć
- nie będziemy o nim mówić zbyt wiele, ale stosuje się go powszechnie (na równi z Gradle)

#### Java - [click](./docs/java.md)
- Oracle od najnowszej javy wprowadza "procent" od aplikacji **komercyjnych**
- my używamy w celach dydatktycznych - można pobrać bezpośrednio javę z linków **unregistered**

#### GIT - [click](./docs/git.md)
- nie będziemy się o nim uczyć
- użyjemy go, abyśmy wszyscy mieli te same wersje plików

### Instrukcja dodawania zmiennych środowiskowych - [click](./docs/add-system-variables.md)
- możliwe, że nie będzie potrzebna po poprawnych instalacjach

### Lista komend w konsoli (w celu sprawdzenia wszystkich narzędzi):
Aby odpalić konsolę, należy kliknąć `"win" + R`, a następnie wpisać `cmd`. Poniższe komendy (lewa strona) należy po kolei przeklejać do konsoli i sprawdzać wynik (prawa strona).
- `javac -version` - javac 1.8.0_191
- `java -version` - java version "1.8.0_191"
- `mvn -v` - Apache Maven 3.3.9
- `git --version` - git version 2.9.0.windows.1

W przypadku pojawienia się informacji podobnej do podanej poniżej:
```
'javac' is not recognized as an internal or external command,
 operable program or batch file.
```
Instalacja, bądź konfiguracja nie jest wykonana poprawnie i należy sprawdzić kroki w sekcji `Uwagi do instalacji`.
       
### Lista komend GIT
1. Pobranie projektu:
    - `git clone [PROJECT_HOST]`
    - np. `git clone https://bitbucket.org/nidckon/java-base.git`
2. Aktualizacja wersji oraz aktualnej **gałęzi**:
    - `git fetch` - ściąga informacje o zmianach
    - `git pull` - ściąga informacje o zmianach i zmiany oraz aktualizuje aktualną **gałąź**

Więcej info o poleceniach - [tutaj](./docs/git-commands.md)

### Przełączanie pomiędzy lekcjami
**Nasze zajęcia będziemy prowadzić zawsze na głównej gałęzi.**

Więcej info o przełączaniu - [tutaj](./docs/switch-lesson.md)


### Tworzenie własnej aplikacji i jej uruchamianie
Informacje o budowaniu aplikacji oraz uruchamianiu znajdziesz - [tutaj](./docs/build-run-app.md)

##
## Spis lekcji - [click](./docs/lessons.md)
#### Stary spis lekcji - nie będziemy ich przerabiać - [click](./docs/legacy_lessons.md)
