package pl.nidckon.java.lekcje;

public class lekcja_6_2_while {
    public static void main(String[] args) {

        int iteracja = 1;

        // for (INICJALIZACJA ; WARUNEK ; DZIALANIE_PO_ITERACJI)
        while (iteracja <= 10) {
            System.out.printf("To jest przejście numer: %d\n", iteracja);
            ++iteracja;
        }

        /* ------------------------------------------- */


        String napis = "";

        while (napis.length() < 4) {
            napis += napis.length();
        }

        System.out.printf("Napis #1 to: %s\n", napis);


        /* ------------------------------------------- */


        napis = "";

        while (!napis.endsWith("4")) {
            napis += napis.length();
        }

        System.out.printf("Napis #2 to: %s\n", napis);


        /* ------------------------------------------- */


        napis = "";

        while (true) {
            if (napis.endsWith("4")) {
                break;
            }
            napis += napis.length();
        }

        System.out.printf("Napis #3 to: %s\n", napis);


        /* ------------------------------------------- */


        napis = "";

        do {
            napis += napis.length();
        } while (napis.length() < 4);

        System.out.printf("Napis #4 to: %s\n", napis);
    }
}
