package pl.nidckon.java.lekcje;

public class lekcja_4_1_tablice {
    public static void main(String[] args) {

        int prymityw;  // 0
        Integer niePrymityw;  // null

        Integer niePrymityw2 = null;

        int nazwaZmiennej = 132;

        int[] tablicaLiczb_pusta = new int[] {};  // pusta tablica

        int[] tablicaLiczb_5 = new int[5]; // pusta tablica z miejscem na 5 elementów

        Integer[] tablicaLiczbCalkowitych_5 = new Integer[5];  // też pusta tablica

        int[] tablicaLiczb_1 = new int[] { 5 };
        // 1 element w tablicy o wartości 5

        int[] tablicaLiczb_2 = new int[] {3, 5}; // 2 elementy w tablicy

        int pozycja = tablicaLiczb_2.length - 1;

        System.out.printf("Ostatni element: %d\n", tablicaLiczb_2[pozycja]);

        int[] tablicaLiczb_3 = {2, 4, 6};
        /*
            tablicaLiczb_3[0]       <---- 2
            tablicaLiczb_3[1]       <---- 4
            tablicaLiczb_3[2]       <---- 6
         */

        System.out.printf("Pierwszy element: %d\n", tablicaLiczb_3[0]);

        tablicaLiczb_3[0] = -20;

        System.out.printf("Pierwszy element: %d\n", tablicaLiczb_3[0]);

        System.out.printf("Rozmiar tablicy: %d\n", tablicaLiczb_3.length);


        System.out.printf("Ostatni element tablicy: %d\n", tablicaLiczb_3[tablicaLiczb_3.length - 1]);


        /*
        Sudoku:
            int poz_0_0 = 1;
            int poz_0_1 = 2;
            ...
            int poz_8_8 = 9;

         ----------------------
         Sudoku:
            int[] pierwsza_kratka = {
                1, 2, 3,
                4, 5, 6,
                7, 8, 9
            };
            ...
            int[] ostatnia_kratka = {
                4, 6, 5,
                9, 8, 7,
                3, 2, 1
            };

         ----------------------

         Sudoku:
            int[][] sudoku = {
                {1, 2, 3,
                 4, 5, 6,               <---- pierwsza kratka
                 7, 8, 9},

                {3, 4, 5,
                 1, 2, 8,               <---- druga kratka
                 7, 6, 9},

                 ....
                 {9, 8, 7,
                  6, 5, 4,              <---- ostatnia kratka
                  3, 2, 1}
            };
         */


        /* -------------------------------------------- */


        int[][] macierz = {
                {2, 3, 4},
                {5, 3, 0}
        };

        System.out.printf("Tablica: %d\n", macierz[0][2]);              // <----- 4

        System.out.printf("Liczba wierszy: %d\n", macierz.length);      // <----- 2
        System.out.printf("Liczba kolumn: %d\n", macierz[0].length);    // <----- 3
    }
}
