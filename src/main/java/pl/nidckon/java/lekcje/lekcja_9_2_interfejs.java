package pl.nidckon.java.lekcje;

import pl.nidckon.java.lekcje.lekcja_9.AnimalCat;
import pl.nidckon.java.lekcje.lekcja_9.AnimalDog;
import pl.nidckon.java.lekcje.lekcja_9.Roar;

public class lekcja_9_2_interfejs {
    public static void main(String[] args) {
        AnimalCat kot = new AnimalCat();
        AnimalDog pies = new AnimalDog();
        Roar papi;

        kot.dajGlos();
        pies.dajGlos();
    }
}
