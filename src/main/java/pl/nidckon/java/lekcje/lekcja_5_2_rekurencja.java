package pl.nidckon.java.lekcje;

import java.util.Scanner;

public class lekcja_5_2_rekurencja {
    static final Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        int liczba = 5;
        Integer liczba2 = 5;
        int tyleRazy = 3;

        int wynik = liczba + liczba + liczba;

        int wynikMetod = dodajTrzyLiczby(liczba, liczba, liczba);

        int wynikRekurencyjny = dodajLiczbeTyleRazy(tyleRazy, liczba);

        System.out.printf("Liczba dodawana recznie: %d\n", wynik);
        System.out.printf("Liczba dodawana przez metody: %d\n", wynikMetod);
        System.out.printf("Liczba dodawana rekurencyjnie: %d\n", wynikRekurencyjny);

        String imie = pobierzImie();
        String imie2 = pobierzWartosc("Podaj imie 2");
        String imie3 = pobierzWartosc("Podaj imie 3");
    }

    public static String pobierzImie() {
        System.out.println("Podaj imie");
        return sc.nextLine();
    }

    public static String pobierzWartosc(String tekstDoPokazania) {
        System.out.println(tekstDoPokazania);
        return sc.nextLine();
    }


    /* ----------------------------------------------- */


    public static int dodajTrzyLiczby(int liczbaPierwsza, int liczbaDruga, int liczbaTrzecia) {
        return liczbaTrzecia + dodajDwieLiczby(liczbaPierwsza, liczbaDruga);
    }

    public static int dodajDwieLiczby(int liczbaPierwsza, int liczbaDruga) {
        return liczbaPierwsza + liczbaDruga;
    }


    /* ----------------------------------------------- */


    public static int dodajLiczbeTyleRazy(int ileRazy, int liczba) {
        if (ileRazy <= 0) {
            return 0;
        } else {
            return liczba + dodajLiczbeTyleRazy(ileRazy - 1, liczba);
        }
    }
}
