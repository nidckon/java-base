package pl.nidckon.java.lekcje;

public class lekcja_2_1_zmienne {
    public static void main(String[] args) {
        boolean logicznaWartoscTak = true;
        boolean logicznaWartoscNie = false;

        char znak = 'a';
        char znakSpecjalny = '\n';
        char znakSpecjalnyEnter_liczba = 13;

        String napis = "napis";
        String napisZeZnakamiSpecjalnymi = "Napis ze znakami specjalnymi!\n";

        int liczba = 14;
        int liczbaUjemna = -15;

        double wartoscPi = 3.14;
        double liczbaUjemnaZmiennoprzecinkowa = -5.124;
        double liczbaBezWartosciCalkowitych = .99;  // 0.99
        double liczbaBezReszty = 1.;  // 1.0
    }
}
