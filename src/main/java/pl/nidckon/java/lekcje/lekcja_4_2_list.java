package pl.nidckon.java.lekcje;

import pl.nidckon.java.informacje.UserFullData;
import pl.nidckon.java.informacje.UserPreviewData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class lekcja_4_2_list {
    public static void main(String[] args) {

        // 'lista tablicowa' - dynamiczny rozmiar
        ArrayList<Integer> listaLiczb = new ArrayList<>();
        // dodawanie elementów na koniec listy
        listaLiczb.add(5);  // 5
        listaLiczb.add(6);  // 5, 6
        listaLiczb.add(10);  // 5, 6, 10
        listaLiczb.add(8);  // 5, 6, 10, 8

        List<Integer> listaLiczb2 = new ArrayList<>();
        List<String> listaWyrazow = new ArrayList<>();

        // UserPreviewData - podstawowe dane
        // UserFullData - podstawowe dane + dodatkowe dane
        UserFullData userFullData = new UserFullData();
        UserPreviewData userPreviewData = new UserFullData();


        // 3 sposoby stworzenia niemodyfikowalnych List - elementy tam są stałe
        List<Integer> lista2 = Arrays.asList(1, 2);
        List<Integer> lista3 = Collections.emptyList();
        List<Integer> lista4 = Collections.singletonList(5);


        // tablica o stałym rozmiarze
        Integer[] tablicaLiczb = new Integer[2];


        int rozmiarListy = listaLiczb.size();
        int rozmiarTablicy = tablicaLiczb.length;

        // dodawanie elementu
        listaLiczb.add(3);
        tablicaLiczb[0] = 3;

        // dodawanie elementu na konkretnej pozycji
        listaLiczb.add(0, 80);
        tablicaLiczb[0] = 5;

        // usunięcie elementu z konkretnej pozycji
        listaLiczb.remove(0);
        tablicaLiczb[0] = null;  // wyczyszczenie elementu, przypisanie 'nic' do konkretnej pozycji

        // wyczyszczenie całej listy
        listaLiczb.clear();


        // wszystkie elementy "nieprymitywne" są obiektami - typ Object
        ArrayList<Object> tablicaWszystkiego = new ArrayList<>();
        tablicaWszystkiego.add(14);
        tablicaWszystkiego.add("thing");
        tablicaWszystkiego.add(true);
    }
}
