package pl.nidckon.java.lekcje;

public class lekcja_3_2_konwersja {
    public static void main(String[] args) {
        float a = 96;
        double b = 97;
        char enter = 13;
        int c = 'c';

        char enter2 = (char) 13;
        int d = (int) 99.0;

        float e = ((int) 3.9f) * 4;  // 12.0
        float f = 3.9f * 4;  // 15.6

        /* -------------------------------------- */

        Integer integerA = 96;
        Integer integerB = new Integer(97);
        Integer integerC = new Integer("98");
        int wartosc = 9 * 10 + 8;  // 98

        int primitiveA = new Integer(96);

        Float floatA = 96f;
        Double doubleA = 96.;
        Boolean booleanTrue = true;
        String wartoscBoolean = booleanTrue.toString();  // "true"
        Character characterA = 'a';
    }
}
