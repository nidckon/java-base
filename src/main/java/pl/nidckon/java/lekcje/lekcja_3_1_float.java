package pl.nidckon.java.lekcje;

public class lekcja_3_1_float {
    public static void main(String[] args) {
        float wartoscPi = 3.14f;
        float liczbaUjemnaZmiennoprzecinkowa = -5.124f;
        float liczbaBezWartosciCalkowitych = .99f;  // 0.99
        float liczbaBezReszty = 1.f;  // 1.0

        float dodawanieLiczbyCalkowitej = 3.14f + 5;
        float dodawanieLiczbyZmiennoprzecinkowej = 3.99f + 4.01f;
        float odejmowanieReszty = 2.99f - .99f;  // 2.0

        double doubleDiv3 = 10. / 3.;
        float floatDiv3 = 10.f / 3.f;

        System.out.printf("Double: %.8f\n", doubleDiv3);
        System.out.printf("Float: %.8f\n", floatDiv3);

        Float wartosc = Float.valueOf("123.5");
        System.out.printf("Wartość floata: %.8f\n", wartosc);
    }
}
