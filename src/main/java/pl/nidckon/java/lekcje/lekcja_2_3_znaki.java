package pl.nidckon.java.lekcje;

public class lekcja_2_3_znaki {
    public static void main(String[] args) {
        char znak = 'a';
        char znakSpecjalny = '\n';
        char znakSpecjalnyEnter_liczba = 13;


        char kolejnaLiteraPoA = 'a' + 1;
        char literaPrzedB = 'b' - 1;

        // Character - nie-prymityw

        System.out.println(znak);

        /* ------------------------------------------ */

        String napis = "napis";
        String napisZeZnakamiSpecjalnymi = "Napis ze znakami specjalnymi!\n";

        String napisLaczony = "Napis" + " laczony";
        String napisFormatowany = String.format("%s %s", "Hello", "World");

        String liczba13 = String.valueOf(13);  // "13"

        System.out.println(liczba13);
        System.out.println(napisZeZnakamiSpecjalnymi);
    }
}
