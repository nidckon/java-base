package pl.nidckon.java.lekcje;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class lekcja_7_3_plik_zapisywanie {
    public static void main(String[] args) throws IOException {
        File path = new File("./docs/samples");


        File file = new File(path, "sample_file_1.txt");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();

        FileWriter fileWriter = new FileWriter(file);

        fileWriter.write("Pierwsza linia!\n");
        fileWriter.write("I ostatnia linia");

        fileWriter.close();


        /* ----------------------------- */

        file = new File(path, "sample_file_2.txt");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        PrintWriter printWriter = new PrintWriter(file);

        printWriter.println("Pierwsza linia!");
        printWriter.print("I ostatnia linia w drugim pliku!");

        printWriter.close();

    }
}
