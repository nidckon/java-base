package pl.nidckon.java.lekcje;

public class lekcja_5_1_if {
    public static void main(String[] args) {

        boolean pokazacNapis = false;

        if (pokazacNapis) {
            System.out.println("Pokażę ten napis, jeśli spełniony warunek!");
        }


        /* ----------------------------------------------------------- */

        /*
1. Utworzyć scanner, aby wczytać wartość od użytkownika
2. Wyświetlić napis "podaj swój wiek"
3. Wczytać liczbę całkowitą podaną przez użytkownika
4. uzupełnić zmienną "czyJestemPelnoletni" o wartość podaną przez użytkownika
5. uruchomić program
         */

        boolean czyJestemPelnoletni = 15 >= 18;
        /*
            A < B   -> prawda jeśli A mniejsze od B
            A > B   -> prawda, jeśli A większe od B
            A <= B  -> prawda, jeśli A mniejsze lub równe od B
            A >= B  -> prawda, jeśli A większe lub równe od B
            A == B  -> prawda, jeśli A równe B
            A != B  -> prawda, jeśli A różne od B
         */

        if (czyJestemPelnoletni) {
            System.out.println("Tak jestem!");
        } else {
            System.out.println("Nie jestem :(");
        }


        /* ----------------------------------------------------------- */


        String KOBIETA = "kobieta";
        String MEZCZYZNA = "mezczyzna";
        String INNE = "inne";

        String plec = MEZCZYZNA;

        if (KOBIETA.equals(plec)) {
            System.out.println("Kopernik też była kobietą!");
        } else if (MEZCZYZNA.equals(plec)) {
            System.out.println("Jesteś facetem!");
        } else {
            System.out.println("Nie wiesz, kim jesteś...?");
        }


        if (KOBIETA.equals(plec)) {
            System.out.println("Kopernik też była kobietą!");
        } else if (MEZCZYZNA.equals(plec)) {
            System.out.println("Jesteś facetem!");
        }


        /* ----------------------------------------------------------- */


        int liczba = 54;

        if (liczba > 30 && liczba <= 39) {
            // (30, 39]
            System.out.printf("Liczba %d jest większa od 30 i mniejsza (lub równa) 39\n", liczba);
        } else if (liczba >= -100) {
            // [-100, +oo]
            System.out.println("Liczba jest większa lub równa '-100'!");
        } else if (0 > liczba || 0 < liczba) {
            // liczba > 0 ALBO liczba < 0
            System.out.println("Podana liczba większa lub mniejsza od ZERO");
        } else if (liczba != 0) {
            // liczba <> 0
            System.out.println("Podana liczba jest różna od ZERA");
        } else if (!(liczba > 50)) {
            System.out.println("Liczba nie większa od 50");
        }
    }
}
