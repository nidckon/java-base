package pl.nidckon.java.lekcje;

import java.util.Scanner;

public class lekcja_2_5_czytanie {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        scanner.hasNextBoolean();
        scanner.nextBoolean();

        scanner.hasNext();
        scanner.next();

        scanner.hasNextLine();
        scanner.nextLine();

        scanner.hasNextInt();
        scanner.nextInt();

        scanner.hasNextDouble();
        scanner.nextDouble();

        scanner.close();
    }
}
