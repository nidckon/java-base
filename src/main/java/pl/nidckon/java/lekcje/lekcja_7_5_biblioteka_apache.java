package pl.nidckon.java.lekcje;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class lekcja_7_5_biblioteka_apache {
    public static void main(String[] args) throws IOException {
        File file = new File("./docs/samples/lista.txt");

        String zawartoscPliku = FileUtils.readFileToString(file, Charsets.UTF_8);

        System.out.println("Plik ma treść:\n" + zawartoscPliku);

        /* ------------------------------------------------------- */

        /*
            Placeholder pod wykorzystanie innej biblioteki
         */
    }
}
