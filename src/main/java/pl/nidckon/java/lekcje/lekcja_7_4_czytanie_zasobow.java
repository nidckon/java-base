package pl.nidckon.java.lekcje;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class lekcja_7_4_czytanie_zasobow {
    public static void main(String[] args) throws IOException {

        URL resource = lekcja_7_4_czytanie_zasobow.class.getResource("zasob.txt");

        Scanner scanner = new Scanner(resource.openStream());

        while (scanner.hasNextLine()) {
            System.out.println("Linia: " + scanner.nextLine());
        }

        scanner.close();


        /* ----------------------------------------------------- */


        URL resource1 = lekcja_7_4_czytanie_zasobow.class.getClassLoader().getResource("list.txt");

        scanner = new Scanner(resource1.openStream());

        while (scanner.hasNextLine()) {
            System.out.println("Linia: " + scanner.nextLine());
        }

        scanner.close();
    }
}
