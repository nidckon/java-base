package pl.nidckon.java.lekcje;

public class lekcja_1_2_pokazanie {
    public static void main(String[] args) {

        System.out.println("Hello world");
        System.out.print("Hello world\n");
        System.out.printf("Hello world\n");
        System.out.printf("%s\n", "Hello world");
        System.out.printf("%s %s\n", "hello", "world");

        /* blok komentarza

        %s - ciąg znaków
        %f - liczby z przecinkiem

        %.2f - liczby z przecinkiem, do 2 miejsc po przecinku

        %d - liczby całkowite

         */

        System.out.println("nie komentarz"); // komentarz
    }
}
