package pl.nidckon.java.lekcje;

public class lekcja_6_1_switch {
    public static void main(String[] args) {
        String pokazacNapis = "TAK";

        switch (pokazacNapis) {
            case "TAK": {
                System.out.println("Pokażę ten napis, jeśli spełniony warunek!");
                break;
            }
        }


        /* ----------------------------------------------------------- */


        int liczba = 1;

        switch (liczba) {
            case 1: {
                System.out.println("Liczba to 1!");
                break;
            }
            case 2: {
                System.out.println("Liczba to 2!");
                break;
            }
            case -1: {
                System.out.println("Liczba to -1!");
            }
        }

        /* ----------------------------------------------------------- */


        String KOBIETA = "kobieta";
        String MEZCZYZNA = "mezczyzna";
        String INNE = "inne";

        String plec = KOBIETA;

        switch (plec) {
            case "kobieta": {
                System.out.println("Jesteś kobietą!");
                break;
            }
            case "mezczyzna": {
                System.out.println("Jesteś mężczyzną!");
                break;
            }
            default: {
                System.out.println("Jesteś czymś innym, niż kobieta lub mężczyzna!");
            }
        }


        /* ----------------------------------------------------------- */


        int pokazOd = 2;

        switch (pokazOd) {
            case 1: {
                System.out.println("1");
            }
            case 2: {
                System.out.println("2");
            }
            case 3: {
                System.out.println("3");
            }
            case 4: {
                System.out.println("4");
            }
            default:
                System.out.println("koniec");
        }
    }
}
