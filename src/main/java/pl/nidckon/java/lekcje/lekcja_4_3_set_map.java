package pl.nidckon.java.lekcje;

import java.util.*;

public class lekcja_4_3_set_map {
    public static void main(String[] args) {

        // Zestaw unikalnych wartości
        Set<Integer> emptySet = Collections.emptySet();  // niemodyfikowalny
        Set<String> emptySetOfString = Collections.emptySet();  // niemodyfikowalny

        SortedSet<Integer> emptySortedSet = Collections.emptySortedSet();  // niemodyfikowalny

        HashSet<Integer> set = new HashSet<>();  // gotowy do zmian
        set.add(5);
        System.out.println("Rozmiar SET po dodaniu 5: " + set.size());

        set.add(5);
        System.out.println("Rozmiar SET po drugim dodaniu 5: " + set.size());

        set.add(6);
        System.out.println("Rozmiar SET po dodaniu 6: " + set.size());

        ArrayList<String> wszystkieWyrazy = new ArrayList<>();
        wszystkieWyrazy.add("Masło");
        wszystkieWyrazy.add("Chlebaczek");
        wszystkieWyrazy.add("Nóż");
        wszystkieWyrazy.add("Drukarka");
        wszystkieWyrazy.add("Drukarka");
        wszystkieWyrazy.add("Drukareczka");
        System.out.println("Lista wyrazów - rozmiar: " + wszystkieWyrazy.size());

        Set<String> unikalneWyrazy = new HashSet<>();
        unikalneWyrazy.addAll(wszystkieWyrazy);
        System.out.println("Set unikalnych wyrazów - rozmiar: " + unikalneWyrazy.size());

        /* ------------------------------------ */

        HashMap<Integer, Integer> map = new HashMap<>();  // do zmiany
        map.put(2, 4);  // [2] = 4
        map.put(2, 8);  // [2] = 8

        map.put(13, 100);  // [2] = 8 ; [13] = 100
        map.put(1, 256);  // [2] = 8 ; [13] = 100 ; [1] = 256

        Map<Integer, Integer> emptyMap = Collections.emptyMap(); // niemodyfikowalne
        Map<Integer, Integer> singleMap = Collections.singletonMap(1, 125);  // niemodyfikowalne


        Set<Integer> klucze = map.keySet();
        Collection<Integer> wartosci = map.values();

        System.out.println("Rozmiar Zestawu kluczy: " + map.keySet().size());
        System.out.println("Rozmiar Wartości: " + map.values().size());
        map.put(1, 8);  // [2] = 8 ; [13] = 100 ; [1] = 8
        System.out.println("Rozmiar Zestawu kluczy: " + map.keySet().size());
        System.out.println("Rozmiar Wartości: " + map.values().size());


        HashMap<String, Integer> mapaWartosci = new HashMap<>();  // do zmiany
    }
}
