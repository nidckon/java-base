package pl.nidckon.java.lekcje;

import pl.nidckon.java.lekcje.lekcja_9.SampleEnum;
import pl.nidckon.java.lekcje.lekcja_9.SampleEnum2;
import pl.nidckon.java.lekcje.lekcja_9.SampleEnum3;

public class lekcja_9_1_enum {
    public static void main(String[] args) {

        SampleEnum czwartek = SampleEnum.valueOf("CZWARTEK");
        SampleEnum czwartek2 = SampleEnum.CZWARTEK;

        if (czwartek == czwartek2) {
            System.out.println("CZWARTEK to CZWARTEK");
        } else {
            System.out.println("CZWARTEK coś nie pasuje");
        }

        if (SampleEnum.WTOREK.equals("WTOREK")) {
            System.out.println("To faktycznie WTOREK");
        } else {
            System.out.println("To nie wtorek!");
        }

        if (SampleEnum.WTOREK.toString().equals("WTOREK")) {
            System.out.println("To jednak WTOREK!");
        } else {
            System.out.println("Mimo wszystko - nie wtorek!");
        }


        /* --------------------------------------------------- */


        SampleEnum2 pon = SampleEnum2.fromShortcut("pon");

        if (pon == SampleEnum2.PONIEDZIALEK) {
            System.out.println("PONIEDZIALEK ze skrótu - działa");
        } else {
            System.out.println("Coś PONIEDZIALEK nie działa");
        }


        /* --------------------------------------------------- */


        SampleEnum3 sobota = SampleEnum3.SOBOTA;

        if (sobota.dzienTygodnia.equals("sobota")) {
            System.out.println("SOBOTA to SOBOTA");
        } else {
            System.out.println("To nie jest SOBOTA!");
        }

        if (sobota.godzinPracy > 0) {
            System.out.println("To pracujący dzień");
        } else {
            System.out.println("To dzień odpoczynku");
        }

    }
}
