package pl.nidckon.java.lekcje;

import pl.nidckon.java.lekcje.lekcja_8.Hermetyzacja;
import pl.nidckon.java.lekcje.lekcja_8.SampleClass;

import java.util.ArrayList;

public class lekcja_8_1_klasa {
    public static void main(String[] args) {
        SampleClass mojObiekt = new SampleClass();

        Hermetyzacja hermetyzacja = new Hermetyzacja();

        hermetyzacja.poleDostepnePozaKlasa = 3;
        int pole = hermetyzacja.pobierzPole();
        System.out.println("Pole #1 (z klasy) ma wartość: " + pole);
        pole = 12;
        System.out.println("Pole #2 (lokalnie) ma wartość: " + pole);
        pole = hermetyzacja.pobierzPole();
        System.out.println("Pole #3 (z klasy) ma wartość: " + pole);
        hermetyzacja.ustalWartosc(20);
        pole = hermetyzacja.pobierzPole();
        System.out.println("Pole #4 ma wartość: " + pole);

        System.out.println();

        ArrayList<Integer> pole2 = hermetyzacja.pobierzPoleDrugie();
        System.out.println("Drugie #1 (z klasy) ma rozmiar: " + pole2.size());
        pole2.add(123);
        System.out.println("Drugie #2 (z lokalnie) ma rozmiar: " + pole2.size());
        pole2 = hermetyzacja.pobierzPoleDrugie();
        System.out.println("Drugie #3 (z klasy) ma rozmiar: " + pole2.size());
        hermetyzacja.ustalWartoscDrugiegoPola(99);
        pole2 = hermetyzacja.pobierzPoleDrugie();
        System.out.println("Drugie #4 (z klasy) ma rozmiar: " + pole2.size());

        hermetyzacja = null;
    }
}
