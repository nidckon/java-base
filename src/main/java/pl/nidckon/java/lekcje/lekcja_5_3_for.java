package pl.nidckon.java.lekcje;

public class lekcja_5_3_for {
    public static void main(String[] args) {


        /*
            iteracja++    ->    iteracja = iteracja + 1;
            ++iteracja    ->    iteracja = iteracja + 1;

            iteracja += 1    ->    iteracja = iteracja + 1;
         */
        for (int iteracja = 1; iteracja <= 10; iteracja++) {
            System.out.printf("To jest iteracja numer: %d\n", iteracja);
        }


        /* ------------------------------------------- */


        System.out.println("1, 2, 3, 4, 5, 6");

        int drugiIterator;
        int doKtorejIteracji = 6;
        int zwiekszO = 2;

        for (drugiIterator = 1; drugiIterator <= doKtorejIteracji; drugiIterator = drugiIterator + zwiekszO) {
            System.out.printf("%d, ", drugiIterator);
        }


        /* ------------------------------------------- */


        int trzeciIterator = 1;

        for (; trzeciIterator < 5; ++trzeciIterator) {
            System.out.printf("\nTrzeci iterator, przejście numer: %d\n", trzeciIterator);
        }


        /* ------------------------------------------- */


        int czwartyIterator = 1;

        for (;;) {
            if (czwartyIterator >= 3) {
                break;
            }
            System.out.printf("To jest przejście numer: %d\n", czwartyIterator);
            czwartyIterator++;
        }



        /* ------------------------------------------- */


        String alfabet;
        char litera = 'a';

        for (alfabet = ""; !alfabet.contains("m"); alfabet += litera) {
            litera++;
        }

        System.out.printf("Alfabet: %s\n", alfabet);
    }
}
