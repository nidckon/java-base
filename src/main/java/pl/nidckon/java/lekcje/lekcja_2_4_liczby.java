package pl.nidckon.java.lekcje;

public class lekcja_2_4_liczby {
    public static void main(String[] args) {
        int liczba = 14;
        int liczbaUjemna = -15;

        int dodawanie = 5 + 8;
        int odejmowanie = 4 - 5;
        int mnozenie = 3 * 2;
        int dzielenie = 9 / 5;  // = 1
        int kolejnoscWykonywaniaDzialan = 2 + 2 * 2;
        int zNawiasami = (2 + 2) * 2;

        int maxLiczba = Integer.max(3, 8);

        System.out.println(maxLiczba);
        System.out.println(kolejnoscWykonywaniaDzialan);

        /* ------------------------------------------ */

        double wartoscPi = 3.14;
        double liczbaUjemnaZmiennoprzecinkowa = -5.124;
        double liczbaBezWartosciCalkowitych = .99;  // 0.99
        double liczbaBezReszty = 1.;  // 1.0

        double dodawanieLiczbyCalkowitej = 3.14 + 5;
        double dodawanieLiczbyZmiennoprzecinkowej = 3.99 + 4.01;
        double odejmowanieReszty = 2.99 - .99;  // 2.0

        double max = Double.max(2.14, 3.14);  // 3.14

        System.out.println(max);
    }
}
