package pl.nidckon.java.lekcje.lekcja_8;

/* definicja klasy
    public - publiczna, dostępna w całej aplikacji oraz poza
    class - słowo klucz dla klasy
    SampleClass - nazwa naszej klasy
*/
public class SampleClass {

    /* konstruktor
        public - dostępna metoda z zewnątrz
     */
    public SampleClass() {

    }
}
