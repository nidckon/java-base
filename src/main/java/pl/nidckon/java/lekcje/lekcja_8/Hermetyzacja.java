package pl.nidckon.java.lekcje.lekcja_8;

import java.util.ArrayList;

public class Hermetyzacja {
    public int poleDostepnePozaKlasa = 5;
    private int poleTylkoWKlasie = 7;
    private ArrayList<Integer> drugiePoleTylkoWKlasie = new ArrayList<>();

    public int pobierzPole() {
        this.wyswietl(String.format("Zwracam wartosc: %d\n", this.poleTylkoWKlasie));
        return this.poleTylkoWKlasie;
    }

    public void ustalWartosc(final int jakasWartosc) {
        wyswietl(String.format("Zmieniam wartosc z %d na %d", poleTylkoWKlasie, jakasWartosc));
        this.poleTylkoWKlasie = jakasWartosc;
    }

    public ArrayList<Integer> pobierzPoleDrugie() {
        this.wyswietl(String.format("Zwracam listę o wielkości: %d", this.drugiePoleTylkoWKlasie.size()));
        return this.drugiePoleTylkoWKlasie;
    }

    public void ustalWartoscDrugiegoPola(final int jakasWartosc) {
        wyswietl(String.format("Dodaję wartość %d\n", jakasWartosc));
        this.drugiePoleTylkoWKlasie.add(jakasWartosc);
    }



    private void wyswietl(final String komunikat) {
        System.out.println("Log: " + komunikat);
    }
}
