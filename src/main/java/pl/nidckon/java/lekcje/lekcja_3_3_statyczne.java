package pl.nidckon.java.lekcje;

public class lekcja_3_3_statyczne {

    static String staticStringValue = "some static string value";
    static String spodnie = "dlugie";
    static String bluzka = "zielona";

    static final String ZALOZYCIEL = "Jan Brzechwa";

    public static void main(String[] args) {
        String mojeSpodnie = lekcja_3_3_statyczne.spodnie;

        boolean prawda = Boolean.TRUE;
        boolean falsz = Boolean.FALSE;

        int maxIntWartosc = Integer.MAX_VALUE;
        String value = lekcja_3_3_statyczne.staticStringValue;

        final int nieDoZmiany = 3;
//        nieDoZmiany = 5;

        final String stalyNapis = "fadfd safds afda asd";
    }
}
