package pl.nidckon.java.lekcje;

import java.io.*;
import java.util.Scanner;

public class lekcja_7_2_plik_czytanie {
    public static void main(String[] args) throws IOException {
        File file = new File("./docs/samples/lista.txt");


        InputStream is = new FileInputStream(file);
        Scanner scanner = new Scanner(is);
        while (scanner.hasNextLine()) {
            System.out.println("Linia: " + scanner.nextLine());
        }
        scanner.close();


        /* ----------------------------- */


        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        while (line != null) {
            System.out.println("Linia: " + line);
            line = bufferedReader.readLine();
        }
        bufferedReader.close();
    }
}
