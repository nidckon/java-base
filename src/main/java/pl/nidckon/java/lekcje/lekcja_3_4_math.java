package pl.nidckon.java.lekcje;

import java.util.Random;

public class lekcja_3_4_math {
    public static void main(String[] args) {
        double pi = Math.PI;

        double random = Math.random();
        System.out.printf("Wylosowano liczbę: %.2f\n", random);

        Random random1 = new Random();
        random1.nextInt(5);
        /*
            -4 .. 9                     // + 4
            0 .. 13
            random1.nextInt(13) - 4;    // - 4
         */

        random1.nextBoolean();
        // U1 - 90% => true     // 90% => 0.9
        random1.nextDouble();  // 0.0 .. 1.0
        // nextDouble() > 0.9 ??
        //      TAK => false
        //      NIE => true


        int a = 14;
        int b = -50;
        int wynik = Math.max(a, b);
        System.out.printf("Max z liczb %d i %d to: %d\n", a, b, wynik);

        int absA = Math.abs(a);
        int absB = Math.abs(b);
        int absWynik = Math.max(absA, absB);
        System.out.printf("Max z wartości bezwględnych liczb |%d|=%d i |%d|=%d to: %d\n", a, absA, b, absB, absWynik);

        long round_0_5 = Math.round(0.5);
        long round_0_51 = Math.round(0.51);
        long round_0_4 = Math.round(0.4);
        System.out.println("Zaokrąglenie typu `round` daje wyniki:");
        System.out.printf("\t round(0.5)=%d\n", round_0_5);
        System.out.printf("\t round(0.51)=%d\n", round_0_51);
        System.out.printf("\t round(0.4)=%d\n", round_0_4);

        // .round -> poprawnie      -> typ zwracany: long           -> w printf: %d
        // .ceil -> w górę          -> typ zwracany: double         -> w printf: %f
        // .floor -> w dół          -> typ zwracany: double         -> w printf: %f
    }
}
