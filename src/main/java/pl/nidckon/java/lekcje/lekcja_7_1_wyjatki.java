package pl.nidckon.java.lekcje;

public class lekcja_7_1_wyjatki {
    public static void main(String[] args) {

        int a = 13;
        int b = 0;

        try {
            int wynik = a / b;
            System.out.println("Wynik to: " + wynik);
        } catch (ArithmeticException exception) {
            System.out.println("Nie można dzielić przez 0...");
        }


        /* -------------------------------------- */


        try {
            int liczba_1 = Integer.parseInt("13");
            int liczba_2 = Integer.parseInt("jakas_liczba");

            int wynik = liczba_1 / liczba_2;
        } catch (NumberFormatException exception) {
            System.out.println("Problem z formatem liczby!");
        } catch (ArithmeticException exception) {
            System.out.println("Problem z dzieleniem przez 0!");
        } catch (Exception exception) {
            System.out.println("Nieznany problem!");
        }
    }
}
