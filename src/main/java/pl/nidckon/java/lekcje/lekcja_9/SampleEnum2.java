package pl.nidckon.java.lekcje.lekcja_9;

public enum SampleEnum2 {
    PONIEDZIALEK,
    WTOREK,
    SRODA,
    CZWARTEK,
    PIATEK;

    public static SampleEnum2 fromShortcut(final String shortcut) {
        final String smallShortcut = shortcut.toLowerCase();
        switch(smallShortcut) {
            case "pon": return PONIEDZIALEK;
            case "wt": return WTOREK;
            case "sr":
            case "śr": return SRODA;
            case "czw": return CZWARTEK;
            case "pt": return PIATEK;
        }
        return null;
    }
}
