package pl.nidckon.java.lekcje.lekcja_9;

public enum SampleEnum3 {
    PONIEDZIALEK(8, "poniedziałek"),
    WTOREK(8, "wtorek"),
    SRODA(8, "środa"),
    SOBOTA(0, "sobota"),
    NIEDZIELA(0, "niedziela");

    public int godzinPracy;
    public String dzienTygodnia;

    SampleEnum3(int godzinPracy, String dzienTygodnia) {
        this.godzinPracy = godzinPracy;
        this.dzienTygodnia = dzienTygodnia;
    }
}
