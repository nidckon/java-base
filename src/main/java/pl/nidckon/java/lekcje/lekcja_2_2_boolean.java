package pl.nidckon.java.lekcje;

public class lekcja_2_2_boolean {
    public static void main(String[] args) {
        boolean logicznaWartoscTak = true;
        boolean logicznaWartoscTNie = false;

        boolean AND_logiczny = true && false;  // false
        boolean OR_logiczny = true || false;  // true
        boolean NOT_logiczny = !false;  // true

        boolean nowaZmienna = logicznaWartoscTak || logicznaWartoscTNie;

        boolean logicalAND = Boolean.logicalAnd(true, true);

        System.out.println(nowaZmienna);
        System.out.println(logicalAND);
    }
}
