package pl.nidckon.java.warsztat.rekurencja.structure;

public abstract class SystemElement {

    private String elementName;

    SystemElement(String name) {
        elementName = name;
    }

    public abstract boolean isDir();

    public String getName() {
        return elementName;
    }
}
