package pl.nidckon.java.warsztat.rekurencja;

import pl.nidckon.java.warsztat.rekurencja.structure.DirectoryElement;
import pl.nidckon.java.warsztat.rekurencja.structure.FileElement;
import pl.nidckon.java.warsztat.rekurencja.structure.SystemElement;

import java.util.List;

public class Sample {
    public static void main(String[] args) {
        /*
        Struktura:
        - nowy_katalog
        - plik.txt
        - katalog_domowy
            - plik.doc
            - plik.xls
            - dokumenty
                - image.jpg


         */

        DirectoryElement dir = new DirectoryElement("struktura");
        DirectoryElement nowy_katalog = new DirectoryElement("nowy_katalog");
        FileElement plik_txt = new FileElement("plik.txt");
        DirectoryElement katalog_domowy = new DirectoryElement("katalog_domowy");
        FileElement plik_doc = new FileElement("plik.doc");
        FileElement plik_xls = new FileElement("plik.xls");
        DirectoryElement dokumenty = new DirectoryElement("dokumenty");
        FileElement image_jpg = new FileElement("image.jpg");

        dir.add(nowy_katalog);
        dir.add(plik_txt);
        dir.add(katalog_domowy);

        katalog_domowy.add(plik_doc);
        katalog_domowy.add(plik_xls);
        katalog_domowy.add(dokumenty);

        dokumenty.add(image_jpg);


        listing(dir, "");
    }

    public static void listing(DirectoryElement directory, String prefix) {
        List<SystemElement> content = directory.getContent();
        for (int i = 0; i < content.size(); ++i) {
            SystemElement element = content.get(i);
            System.out.println(prefix + element.getName());
            if (element.isDir()) {
                DirectoryElement subDirectory = (DirectoryElement) element;
                listing(subDirectory, prefix + "\t");
            }
        }
    }
}
