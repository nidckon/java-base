package pl.nidckon.java.warsztat.rekurencja.structure;

public class FileElement extends SystemElement {
    private String fileContent = "";

    public FileElement(String name) {
        super(name);
    }

    @Override
    public boolean isDir() {
        return false;
    }

    public void setContent(String content) {
        fileContent = content;
    }

    public String getContent() {
        return fileContent;
    }
}
