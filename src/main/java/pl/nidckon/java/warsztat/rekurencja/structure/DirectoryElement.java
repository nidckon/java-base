package pl.nidckon.java.warsztat.rekurencja.structure;

import java.util.ArrayList;
import java.util.List;

public class DirectoryElement extends SystemElement {
    private final List<SystemElement> content = new ArrayList<>();

    public DirectoryElement(String name) {
        super(name);
    }

    @Override
    public boolean isDir() {
        return true;
    }

    public void add(SystemElement element) {
        content.add(element);
    }

    public List<SystemElement> getContent() {
        return content;
    }
}
