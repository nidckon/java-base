package pl.nidckon.java.warsztat.rekurencja_zaawansowana;

import java.io.File;

public class Sample {
    public static void main(String[] args) {
        File file = new File("src/main/java/pl/nidckon/java/warsztat");

        listing(file, "", 0);
    }

    public static void listing(File file, String prefix, int deep) {
        System.out.println(prefix + file.getName());
        if (file.isDirectory()) {
            if (deep >= 4) {
                System.out.println("*** Dalej nie pokaze! ***");
            } else {
                File[] files = file.listFiles();
                for (int i = 0; i < files.length; ++i) {
                    listing(files[i], prefix + "\t", deep + 1);
                }
            }
        }
    }
}
